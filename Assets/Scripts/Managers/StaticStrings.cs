﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public static class StaticStrings 
    {
        // inputs 
        public static string Vertical = "Vertical";
        public static string Horizontal = "Horizontal";
        public static string B= "B";
        public static string A = "A";
        public static string Y = "Y";
        public static string X = "X";
        public static string RT = "RT";
        public static string LT = "LT";
        public static string RB = "RB";
        public static string LB = "LB";
        public static string L = "L";

        //Animator parameters
 
        public static string mirror = "mirror";
        public static string parry_attack = "parry_attack";
    }
}
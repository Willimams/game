﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class EnemigoAdministrador : MonoBehaviour
    {
        public List<EnemigoObjetivo> enemyTargets = new List<EnemigoObjetivo>();

        public EnemigoObjetivo GetEnemigo(Vector3 from)
        {
            EnemigoObjetivo r = null;
            float minDist = float.MaxValue;

            for(int i=0; i< enemyTargets.Count; i++)
            {

                float tDist = Vector3.Distance(from, enemyTargets[i].GetTarget().position);

                if (tDist < minDist)
                {
                    minDist = tDist;
                    r = enemyTargets[i];
                }
            }

            return r;

        }

        public static EnemigoAdministrador singleton;
        private void Awake()
        {
            singleton = this;
        }
    }
}

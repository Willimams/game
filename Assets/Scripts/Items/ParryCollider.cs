﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class ParryCollider : MonoBehaviour
    {
        EstadoAdministrador estados;
        EnemigoEstados enemigoEstados;

        public float maxTimer = 0.6f;
        float timer ;
        public void InitPlayer(EstadoAdministrador st)
        {
            estados = st;
        }

        private void Update()
        {
            if (estados)
            {
                timer += estados.delta;

                if (timer > maxTimer)
                {
                    timer = 0;
                    gameObject.SetActive(false);
                }
            }

            if (enemigoEstados)
            {
                timer += enemigoEstados.delta;

                if (timer > maxTimer)
                {
                    timer = 0;
                    gameObject.SetActive(false);
                }

            }
        }
        public void InitEnemy(EnemigoEstados st)
        {
            enemigoEstados = st;
        }

        private void OnTriggerEnter(Collider other)
        {
            /*
            DamageCollider dc = other.GetComponent<DamageCollider>();
            if (dc == null)
                return;
            */
            if (estados)
            {

                EnemigoEstados e_st = other.transform.GetComponentInParent<EnemigoEstados>();

                if (e_st != null)
                {
                    e_st.CheckForParry(transform.root ,estados);
                }
            }

            if(enemigoEstados)
            {
                // check for player
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class DamageCollider : MonoBehaviour
    {
        EstadoAdministrador estados;

        public void Init(EstadoAdministrador st)
        {
            estados = st;
        }

        private void OnTriggerEnter(Collider other)
        {
            EnemigoEstados enemyestados = other.transform.root.GetComponentInParent<EnemigoEstados>();

            if (enemyestados == null)
                return;
            // do damage
          

            enemyestados.DoDamage(estados.currentAction);
        }
    }
}

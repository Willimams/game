﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class WeaponHook : MonoBehaviour
    {
        public GameObject[] damageCollider;

        public void OpenDamageCollider()
        {
            for (int i = 0; i < damageCollider.Length; i++)
            {
                damageCollider[i].SetActive(true);
            }
        }


        public void CloseDamageCollider()
        {
            for (int i = 0; i < damageCollider.Length; i++)
            {
                damageCollider[i].SetActive(false);
            }
        }

        public void InitDamageColliders(EstadoAdministrador estados)
        {
            for (int i = 0; i < damageCollider.Length; i++)
            {
                damageCollider[i].GetComponent<DamageCollider>().Init(estados);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class EntradaControlador : MonoBehaviour
    {
        float vertical;
        float horizontal;
      //  bool runInput;

        bool b_input;
        bool a_input;
        bool x_input;
        bool y_input;

        bool rb_input;
        float rt_axis;
        bool rt_input;
        bool lb_input;
        float lt_axis;
        bool lt_input;

        bool leftAxis_down;
        bool rightAxis_down;

        float b_timer;
        float rt_timer;
        float lt_timer;

        EstadoAdministrador estados;
        CamaraAdministrador camAdministrador;

        float delta;
        // Start is called before the first frame update
        void Start()
        {
            UI.QuickSlot.singleton.Init();

            estados = GetComponent<EstadoAdministrador>();
            estados.Init();

            camAdministrador = CamaraAdministrador.singleton;
            camAdministrador.Init(estados);
        }

        private void FixedUpdate()
        {
            delta = Time.fixedDeltaTime;
            GetInput();
            UpdateStates();
            estados.FixedTick(delta);
            camAdministrador.Tick(delta);
           
        }
        
        private void Update()
        {
            delta = Time.deltaTime;
            estados.Tick(delta);
            ResetInputNStates();
        }
        
        void GetInput()
        {
            vertical = Input.GetAxis(StaticStrings.Vertical);
            horizontal = Input.GetAxis(StaticStrings.Horizontal);
            b_input = Input.GetButton(StaticStrings.B);
            a_input = Input.GetButton(StaticStrings.A);
            y_input = Input.GetButtonUp(StaticStrings.Y);
            x_input = Input.GetButton(StaticStrings.X);
            rt_input = Input.GetButton(StaticStrings.RT);
            rt_axis = Input.GetAxis(StaticStrings.RT);
            if (rt_axis != 0)
                rt_input = true;

            lt_input = Input.GetButton(StaticStrings.LT);
            lt_axis = Input.GetAxis(StaticStrings.LT);
            if (lt_axis != 0)
                lt_input = true;

            rb_input = Input.GetButton(StaticStrings.RB);
            lb_input = Input.GetButton(StaticStrings.LB);

            rightAxis_down = Input.GetButton(StaticStrings.L);

            if (b_input)
                b_timer += delta;
        }

        void UpdateStates()
        {
            estados.horiozontal = horizontal;
            estados.vertical = vertical;

            Vector3 v = estados.vertical * camAdministrador.transform.forward;
            Vector3 h = estados.horiozontal * camAdministrador.transform.right;
            estados.moveDir = (v + h).normalized;
            float m = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            estados.moveAmount = Mathf.Clamp01(m);

            //  estados.rollInput = b_input; /*Liena funcional*/

            if (x_input)
                b_input = false;

            if (b_input && b_timer > 0.5f)
            {
               estados.run = (estados.moveAmount > 0);
                /* if (estados.moveAmount > 0)  estados.run = true    */
            }
            /*  else{ // estados.run = false; }*/
            if (b_input == false && b_timer > 0 && b_timer < 0.5f)
                estados.rollInput = true;

            estados.itemInput = x_input;
            estados.rt = rt_input;
            estados.lt = lt_input;
            estados.rb = rb_input;
            estados.lb = lb_input;

            if(y_input)
            {
                estados.isTwoHanded = !estados.isTwoHanded;
                estados.HandleTwoHanded();
            }
            if (estados.lockOntarget != null)
            {
                if (estados.lockOntarget.enemigoEstados.isDead)
                {
                    estados.lockOn = false;
                    estados.lockOntarget = null;
                    estados.lockOnTransform = null;
                    camAdministrador.lockon = false;
                    camAdministrador.lockonTarget = null;

                }
            }
            else
            {
                estados.lockOn = false;
                estados.lockOntarget = null;
                estados.lockOnTransform = null;
                camAdministrador.lockon = false;
                camAdministrador.lockonTarget = null;
            }

            if (rightAxis_down)
            {
                estados.lockOn = !estados.lockOn;

                estados.lockOntarget = EnemigoAdministrador.singleton.GetEnemigo(transform.position);
                if (estados.lockOntarget == null)
                    estados.lockOn = false;

           

                camAdministrador.lockonTarget = estados.lockOntarget;
                estados.lockOnTransform = estados.lockOntarget.GetTarget();
                camAdministrador.lockonTransform = estados.lockOnTransform;
                camAdministrador.lockon = estados.lockOn;
            }
        }

        void ResetInputNStates()
        {
            if (b_input == false)
                b_timer = 0;
            if (estados.rollInput)
                estados.rollInput = false;
            if (estados.run)
                estados.run = false;
        }
    }
}

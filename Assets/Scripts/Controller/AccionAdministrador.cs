﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class AccionAdministrador : MonoBehaviour
    {
        public List<Action> actionSlots = new List<Action>();

        public ItemAction consumableItem;

        EstadoAdministrador estados;

        public void Init(EstadoAdministrador st)
        {
            estados = st;
            UpdateActionsOneHanded();


        }

        public void UpdateActionsOneHanded()
        {

            EmptyAllSlots();

            DeepCopyAction(estados.inventarioAdministrador.rightHandWeapon, ActionInput.rb, ActionInput.rb);
            DeepCopyAction(estados.inventarioAdministrador.rightHandWeapon, ActionInput.rt, ActionInput.rt);

            if (estados.inventarioAdministrador.hasLeftHandedWeapon)
            {
                DeepCopyAction(estados.inventarioAdministrador.leftHandWeapon, ActionInput.rb, ActionInput.lb, true);
                DeepCopyAction(estados.inventarioAdministrador.leftHandWeapon, ActionInput.rt, ActionInput.lt, true);
            }
            else
            {
                DeepCopyAction(estados.inventarioAdministrador.rightHandWeapon, ActionInput.lb, ActionInput.lb);
                DeepCopyAction(estados.inventarioAdministrador.rightHandWeapon, ActionInput.lt, ActionInput.lt);
            }
        }

    

        public void DeepCopyAction(Weapon w, ActionInput inp ,ActionInput assign, bool isLeftHand= false )
        {
            Action a = GetAction(assign);
            Action w_a = w.GetAction(w.actions, inp);
            if (w_a == null)
                return;

            a.targetAnim = w_a.targetAnim;
            a.type = w_a.type;
            a.canBeParried = w_a.canBeParried;
            a.chageSpeed = w_a.chageSpeed;
            a.animSpeed = w_a.animSpeed;
            a.canBackStab = w_a.canBackStab;
            a.ovverrideDamageAnim = w_a.ovverrideDamageAnim;
            a.damageAnim = w_a.damageAnim;
      

            if (isLeftHand)
            {
                a.mirror = true;
            }

            DeepCopyWeaponStats(w_a.weaponStats, a.weaponStats);
        }

        public void DeepCopyWeaponStats(WeaponStats from, WeaponStats to)
        {
            to.physical = from.physical;
            to.slash = from.slash;
            to.strike = from.strike;
            to.thrust = from.thrust;
            to.magic = from.magic;
            to.lighting = from.lighting;
            to.fire = from.fire;
            to.dark = from.dark;
        }

        public void UpdateActionsTwoHanded()
        {
            EmptyAllSlots();
            Weapon w = estados.inventarioAdministrador.rightHandWeapon;

            for (int i = 0; i < w.two_handedActions.Count; i++)
            {

                Action a = GetAction(w.two_handedActions[i].input);
                a.targetAnim = w.two_handedActions[i].targetAnim;
                a.type = w.two_handedActions[i].type;
            }
        }

        void EmptyAllSlots()
        {
            for (int i = 0; i < 4; i++)
            {
                Action a = GetAction((ActionInput)i);
                a.targetAnim = null;
                a.mirror = false;
                a.type = ActionType.attack;
            }
        }

        AccionAdministrador()
        {
            for (int i = 0; i < 4; i++)
            {
                Action a = new Action();
                a.input = (ActionInput)i;
                actionSlots.Add(a);
            }

        }

       

        public Action GetActionSlot(EstadoAdministrador st)
        {
            ActionInput a_input = GetActionInput(st);
            return GetAction(a_input);
        }

        Action GetAction(ActionInput inp)
        {
         

            for (int i = 0; i < actionSlots.Count; i++)
            {
                if (actionSlots[i].input == inp)
                    return actionSlots[i];
            }

            return null;
        }

        public ActionInput GetActionInput (EstadoAdministrador st)
        {
       
            if (st.rb)
                return ActionInput.rb;
            if (st.rt)
                return ActionInput.rt;
         
            if (st.lb)
                return ActionInput.lb;
            if (st.lt)
                return ActionInput.lt;

            return ActionInput.rb;
        }

        public bool IsLeftHandSlot(Action slot)
        {
            return (slot.input == ActionInput.lb || slot.input == ActionInput.lt);
        }
    }

    public enum ActionInput
    {
       rb,lb,rt,lt
    }

    public enum ActionType
    {
        attack,block,spells,parry
    }

    [System.Serializable]
    public class Action
    {
        public ActionInput input;
        public ActionType type;
        public string targetAnim;
        public bool mirror=false;
        public bool canBeParried = true;
        public bool chageSpeed = false;
        public float animSpeed = 1;
        public bool canBackStab = false;

        public bool ovverrideDamageAnim;
        public string damageAnim;

        public WeaponStats weaponStats;
       
    }

    [System.Serializable]
    public class ItemAction
    {
        public string targetAnim;
        public string item_id;
    }
}

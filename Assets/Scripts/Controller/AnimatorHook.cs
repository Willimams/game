﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class AnimatorHook : MonoBehaviour
    {
        Animator anim;
        EstadoAdministrador estados;
        EnemigoEstados enemigoEstados;
        Rigidbody rigid;
    

        public float rm_multi;
        bool rolling;
        float roll_t;
        float delta;
        AnimationCurve roll_curve;

        public void Init(EstadoAdministrador st, EnemigoEstados eSt)
        {
            estados = st;
            enemigoEstados = eSt;
            if (st != null)
            {
                anim = st.anim;
                rigid = st.rigid;
                roll_curve = estados.roll_curve;
                delta = st.delta;
            }
            if (eSt != null)
            {
              
                anim = eSt.anim;
                rigid = eSt.rigid;
                delta = eSt.delta;
            }
        }

        public void InitForRoll()
        {
            rolling = true;
            roll_t = 0;

        }

        public void CloseRoll()
        {
            if (rolling == false)
                return;

            rm_multi = 1;

            rolling = false;
        }
        private void OnAnimatorMove()
        {
            if (estados == null && enemigoEstados==null)
                return;

            if (rigid == null)
                return;
            ;

            if (estados != null)
            {
                if (estados.canMove)
                    return;

                delta = estados.delta;
            }

            if (enemigoEstados != null)
            {
                if (enemigoEstados.canMove )
                    return;

                delta = enemigoEstados.delta;
            }

            rigid.drag = 0;

            if (rm_multi == 0)
                rm_multi = 1;

            if (rolling == false)
            {
                Vector3 delta2 = anim.deltaPosition;
                delta2.y = 0;
                Vector3 v = (delta2 * rm_multi) / delta;
                rigid.velocity = v;
                //estados.rigid.velocity = v;
            }
            else
            {
              
                roll_t +=  delta / 0.6f;
              
                if (roll_t > 1)
                {
                    roll_t = 1;
                }

                if (estados == null)
                    return;

                float zValue = roll_curve.Evaluate(roll_t);
                Vector3 v1 = Vector3.forward * zValue;
                Vector3 relative = transform.TransformDirection(v1);
                Vector3 v2 = (relative * rm_multi) / estados.delta;
                rigid.velocity = relative;
            }
        }

        public void OpenDamageColliders()
        {
            if (estados)
            {
                estados.inventarioAdministrador.OpenAllDamageColliders();
            }
                OpenParryFlag();
        }

        public void CloseDamageColliders()
        {
            if (estados)
            {
               estados.inventarioAdministrador.CloseAllDamageColliders();
            }
            CloseParryFlag();
        }

        public void OpenParryCollider()
        {
            if (estados == null)
                return;
            estados.inventarioAdministrador.OpenParryColliders();
            
        }

        public void CloseParryCollider()
        {
            if (estados == null)
                return;
            estados.inventarioAdministrador.CloseParryColliders();
           
        }

        public void OpenParryFlag()
        {
            if (estados)
            {
                estados.parryIsOn = true;
            }
            if (enemigoEstados)
            {
                enemigoEstados.parryIsOn = true;
            }
        }

        public void CloseParryFlag()
        {
            if (estados)
            {
                estados.parryIsOn = false;
            }
            if (enemigoEstados)
            {
                enemigoEstados.parryIsOn = false;
            }

        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class EnemigoObjetivo : MonoBehaviour
    {
        public int index;
        public List<Transform> targets = new List<Transform>();
        public List<HumanBodyBones> h_bones = new List<HumanBodyBones>();

        public EnemigoEstados enemigoEstados;

        Animator anim;

        public void Init(EnemigoEstados st)
        {
            enemigoEstados = st;
            anim = enemigoEstados.anim;
            if (anim.isHuman == false)
                return;

            for(int i=0; i< h_bones.Count;i++)
            {
                targets.Add(anim.GetBoneTransform(h_bones[i]));
            }

            EnemigoAdministrador.singleton.enemyTargets.Add(this);
        }

        public Transform GetTarget(bool negative=false)
        {
            if (targets.Count == 0)
                return transform;

            int targetIndex = index;

            if (negative == false)
            {

                if (index < targets.Count - 1)
                {
                    index++;
                }
                else
                {
                    index = 0;
                    targetIndex = 0;
                }
            }
            else
            {
                if (index < 0)
                {
                    index = targets.Count - 1;
                    targetIndex = targets.Count - 1;
                }
                else
                {
                    index--;
                }
            }
            return targets[targetIndex];
        }
    }
}

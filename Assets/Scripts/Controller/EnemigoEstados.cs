﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class EnemigoEstados : MonoBehaviour
    {
        public int health;

        public PersonajeEstados characterStats;

        public bool canBeParried=true;
        public bool parryIsOn = true;
      //  public bool doParry = false;
        public bool isInvicible;
        public bool dontDoAnything;
        public bool canMove;
        public bool isDead;

        public  EstadoAdministrador parriedBy;
        

       public Animator anim;

        EnemigoObjetivo enTarget;
        AnimatorHook a_hook;
        public Rigidbody rigid;
        public float delta;
        public float poiseDegrate = 2;

        List<Rigidbody> ragdollsRigids = new List<Rigidbody>();
        List<Collider> ragdollColliders = new List<Collider>();

        float timer;

        private void Start()
        {
            health = 100000;
            anim = GetComponentInChildren<Animator>();
            enTarget = GetComponent<EnemigoObjetivo>();
            enTarget.Init(this);

            rigid = GetComponent<Rigidbody>();


            a_hook = anim.GetComponent<AnimatorHook>();
            if (a_hook == null)
                a_hook = anim.gameObject.AddComponent<AnimatorHook>();
            a_hook.Init(null,this);

            InitRagdoll();
            parryIsOn = false;
        }

        void InitRagdoll()
        {
            Rigidbody[] rigs = GetComponentsInChildren<Rigidbody>();

            for (int i = 0; i<rigs.Length ; i++)
            {
                if (rigs[i] == rigid)
                    continue;

                ragdollsRigids.Add(rigs[i]);
                rigs[i].isKinematic = true;

                Collider col = rigs[i].gameObject.GetComponent<Collider>();
                col.isTrigger = true;
                ragdollColliders.Add(col);
            }
         }

        public void EnableRagdoll()
        {
           
            for (int i = 0; i < ragdollsRigids.Count; i++)
            {
                ragdollsRigids[i].isKinematic = false;
                ragdollColliders[i].isTrigger = false;
            }

            Collider controllerCollider = rigid.gameObject.GetComponent<Collider>();
            rigid.isKinematic = true;

            StartCoroutine("CloseAnimator");
        }

        IEnumerator CloseAnimator()
        {
            yield return new WaitForEndOfFrame();
            anim.enabled = false;
            this.enabled = false;
        }

        private void Update()
        {
            delta = Time.deltaTime;
            canMove= anim.GetBool("canMove");

            if (dontDoAnything)
            {
                dontDoAnything = !canMove;

                return;
            }

            if (health <= 0)
            {
                if (!isDead)
                {
                    isDead = true;
                    EnableRagdoll();
                }
            }

            if (isInvicible)
            {

                isInvicible = !canMove;
            }

            if(parriedBy != null && parryIsOn==false)
            {
            //    parriedBy.parryTarget = null;
                parriedBy = null;


            }

            if (canMove )
            {
                parryIsOn = false;
                anim.applyRootMotion = false;

                //Debug
                timer += Time.deltaTime;
                if (timer > 3)
                {
                    DoAction();
                    timer = 0;
                }
            }
            characterStats.poise -= delta*poiseDegrate;
            if (characterStats.poise < 0)
                characterStats.poise = 0;
        }

        void DoAction()
        {
            
            anim.Play("oh_attack_1");
            anim.applyRootMotion = true;
            anim.SetBool("canMove", false);
        }
        public void DoDamage(Action a)
        {
            if (isInvicible)
                return;

            int damage = StatsCalculations.CalcualteBaseDamage(a.weaponStats,characterStats);

            characterStats.poise += damage;
            health -= damage;

            if(canMove || characterStats.poise > 100)
            {

                if (a.ovverrideDamageAnim)
                    anim.Play(a.damageAnim);
                else
                {
                    int ran = Random.Range(0,100);
                    string tA = (ran > 50) ? "damage_1" : "damage_2";
                    anim.Play(tA);
                }
            }

            Debug.Log(" Damage is " +  damage + " Poise is " + characterStats.poise);

       
            //health -= v;
            isInvicible = true;

           
            anim.Play("damage_1");
            anim.applyRootMotion = true;
            anim.SetBool("canMove", false);
        }

        public void CheckForParry(Transform target, EstadoAdministrador estados)
        {
            //|| parryIsOn == false //(posible error por el cual no se repoduce la abimacion)
            if (canBeParried==false || isInvicible)
                return ;

            Vector3 dir = transform.position - target.position;
            dir.Normalize();
            float dot = Vector3.Dot(target.forward,dir);
            if (dot < 0)
                return ;

            isInvicible = true;
            anim.Play("attack_interrupt");
            anim.applyRootMotion = true;
            anim.SetBool("canMove", false);
         
            parriedBy = estados;
            return ;
        }

        public void IsGettingParried(WeaponStats weaponStats )
        {
            int damage = StatsCalculations.CalcualteBaseDamage(weaponStats, characterStats);
            health -= damage;
            // health -= 500;
            dontDoAnything = true;
            anim.SetBool("canMove", false);
            anim.Play("parry_recieved");
        }

        public void IsGettingBacktabbed(WeaponStats weaponStats)
        {
            int damage = StatsCalculations.CalcualteBaseDamage(weaponStats, characterStats);
            health -= damage;
            dontDoAnything = true;
            anim.SetBool("canMove", false);
            anim.Play("getting_backstabbed");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class InventarioAdministrador : MonoBehaviour
    {
        public Weapon rightHandWeapon;
        public bool hasLeftHandedWeapon = true;
        public Weapon leftHandWeapon;

        public GameObject parryCollider;

        EstadoAdministrador estados;

        public void Init(EstadoAdministrador st)
        {
            estados = st;
      
            EquipWeapon(rightHandWeapon, false);
           EquipWeapon(leftHandWeapon, true);
            //EquipRightWeapon();
            //EquipLeftHandWeapon();
            InitAllDaamageColliders(st);
            CloseAllDamageColliders();

            ParryCollider pr = parryCollider.GetComponent<ParryCollider>();
            pr.InitPlayer(st);
            CloseParryColliders();
        }

        public void EquipWeapon(Weapon w, bool isLeft = false)
        {
            string targetIdle = w.oh_idle;
            targetIdle += (isLeft) ? "_l" : "_r";
            estados.anim.SetBool("mirror", isLeft);
            estados.anim.Play("changeWeapon");
            estados.anim.Play(targetIdle);

            UI.QuickSlot uiSlot = UI.QuickSlot.singleton;
            uiSlot.UpdateSlot(
                (isLeft) ?
                UI.QSlotType.lh : UI.QSlotType.rh, w.icon);

            if (isLeft)
            {


            }
        }
        public Weapon GetCurrentWeapon(bool isLeft)
        {
            if (isLeft)
                return leftHandWeapon;
            else
                return rightHandWeapon;
        }

        /*
        public void EquipRightWeapon()
        {
            string targetIdle = rightHandWeapon.oh_idle;
            targetIdle += "_r";

            estados.anim.SetBool("mirror",false);
            estados.anim.Play("changeWeapon");
            estados.anim.Play(targetIdle);
        }
        

        public void EquipLeftHandWeapon()
        {
            if (hasLeftHandedWeapon == false)
                return;
            string targetIdle = leftHandWeapon.oh_idle;
            targetIdle += "_l";

            estados.anim.SetBool("mirror",true);
            estados.anim.Play("changeWeapon");
            estados.anim.Play(targetIdle);
        }

        */
        public void OpenAllDamageColliders()
        {
            if (rightHandWeapon.w_hook != null)
                rightHandWeapon.w_hook.OpenDamageCollider();

            if (leftHandWeapon.w_hook != null)
                leftHandWeapon.w_hook.OpenDamageCollider();
        }

        public void CloseAllDamageColliders()
        {
            if (rightHandWeapon.w_hook != null)
                rightHandWeapon.w_hook.CloseDamageCollider();

            if (leftHandWeapon.w_hook != null)
                leftHandWeapon.w_hook.CloseDamageCollider();
        }

        public void InitAllDaamageColliders(EstadoAdministrador estados)
        {
            if (rightHandWeapon.w_hook != null)
                rightHandWeapon.w_hook.InitDamageColliders(estados);

            if (leftHandWeapon.w_hook != null)
                leftHandWeapon.w_hook.InitDamageColliders(estados);
        }

        public void CloseParryColliders()
        {
            parryCollider.SetActive(false);
        }

        public void OpenParryColliders()
        {
            parryCollider.SetActive(true);
        }
    }

    [System.Serializable]
    public class Weapon
    {
        public string weaponId;
        public Sprite icon;
        public string oh_idle;
        public string th_idle;

        public List<Action> actions;
        public List<Action> two_handedActions;
        public WeaponStats parryStats;
        public WeaponStats backstabStats;
        public bool LeftHandMirror;

        public GameObject weaponModel;
        public WeaponHook w_hook;

        
        public Action GetAction(List<Action> l ,ActionInput inp)
        {
            for(int i=0; i<l.Count ;i++)
            {
                if (l[i].input == inp)
                {
                    return l[i];
                }

            }

            return null;
        }
        
    }
}